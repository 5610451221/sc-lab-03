package controller;



import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;


import view.AsciiFrame;
import model.Url;

public class Controller {
	
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}
	
	public static void main(String[] args) {
		new Controller();
		
		// TODO Auto-generated method stub

	}
	
	
	public Controller(){
		frame = new AsciiFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(740, 660);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
		
		
	}
	
	
	public void setTestCase() {
		Url url1 = new Url("https://www.facebook.com/");
		castToAscii(url1, "https://www.facebook.com/");
		frame.setResult("URL :" + "1." + "  " + url1.getCharacter() + "    " + "Total Ascii : "+ "   " +url1.getTotal()+" " );
		totalUrl(url1);
		frame.extendResult("Group : "+ "   " +url1.getTotal()+" " + "\n");
		
		
		Url url2 = new Url("http://game.siamha.com/name/takoyaki/");
		castToAscii(url2, "http://game.siamha.com/name/takoyaki/");
		frame.extendResult("URL :" + "2." + "  " + url2.getCharacter() + "    " + "Total Ascii : "+ "   " +url2.getTotal()+" " );
		totalUrl(url2);
		frame.extendResult("Group : "+ "   " +url2.getTotal()+" " + "\n");
		
		
		Url url3 = new Url("https://www.youtube.com/");
		castToAscii(url3, "https://www.youtube.com/");
		frame.extendResult("URL :" + "3." + "  " + url2.getCharacter() + "    " + "Total Ascii : "+ "   " +url3.getTotal()+" " );
		totalUrl(url3);
		frame.extendResult("Group : "+ "   " +url3.getTotal()+" " + "\n");
		
		
		Url url4 = new Url("https://www.facebook.com/MayTrUmPeT?fref=tl_fr_box&pnref=lhc.friends");
		castToAscii(url4, "https://www.facebook.com/MayTrUmPeT?fref=tl_fr_box&pnref=lhc.friends");
		frame.extendResult("URL :" + "4." + "  " + url4.getCharacter() + "    " + "Total Ascii : "+ "   " +url4.getTotal()+" " );
		totalUrl(url4);
		frame.extendResult("Group : "+ "   " +url4.getTotal()+" " + "\n");
		
		Url url5 = new Url("https://www.facebook.com/ployy.phuaksomon");
		castToAscii(url5, "https://www.facebook.com/ployy.phuaksomon");
		frame.extendResult("URL :" + "5." + "  " + url5.getCharacter() + "    " + "Total Ascii : "+ "   " +url5.getTotal()+" " );
		totalUrl(url5);
		frame.extendResult("Group : "+ "   " +url5.getTotal()+" " + "\n");
		
		
		Url url6 = new Url("http://www.cs.sci.ku.ac.th/~fscichj/ ");
		castToAscii(url6, "http://www.cs.sci.ku.ac.th/~fscichj/ ");
		frame.extendResult("URL :" + "6." + "  " + url6.getCharacter() + "    " + "Total Ascii : "+ "   " +url6.getTotal()+" " );
		totalUrl(url6);
		frame.extendResult("Group : "+ "   " +url6.getTotal()+" " + "\n");
		
		
		Url url7 = new Url("https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit");
		castToAscii(url7, "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit");
		frame.extendResult("URL :" + "7." + "  " + url7.getCharacter() + "    " + "Total Ascii : "+ "   " +url7.getTotal()+" " );
		totalUrl(url7);
		frame.extendResult("Group : "+ "   " +url7.getTotal()+" " + "\n");
		
		Url url8 = new Url("https://twitter.com/");
		castToAscii(url8, "https://twitter.com/");
		frame.extendResult("URL :" + "8." + "  " + url8.getCharacter() + "    " + "Total Ascii : "+ "   " +url8.getTotal()+" " );
		totalUrl(url8);
		frame.extendResult("Group : "+ "   " +url8.getTotal()+" " + "\n");
		
		Url url9 = new Url("https://std.regis.ku.ac.th/");
		castToAscii(url9, "https://std.regis.ku.ac.th/");
		frame.extendResult("URL :" + "9." + "  " + url9.getCharacter() + "    " + "Total Ascii : "+ "   " +url9.getTotal()+" " );
		totalUrl(url9);
		frame.extendResult("Group : "+ "   " +url9.getTotal()+" " + "\n");
		
		Url url10 = new Url("https://www.edmodo.com/");
		castToAscii(url10, "https://www.edmodo.com/");
		frame.extendResult("URL :" + "10." + "  " + url10.getCharacter() + "    " + "Total Ascii : "+ "   " +url10.getTotal()+" " );
		totalUrl(url10);
		frame.extendResult("Group : "+ "   " +url10.getTotal()+" " + "\n");
		
		Url url11 = new Url("http://learnenglish.britishcouncil.org/en/listen-and-watch");
		castToAscii(url11, "http://learnenglish.britishcouncil.org/en/listen-and-watch");
		frame.extendResult("URL :" + "11." + "  " + url11.getCharacter() + "    " + "Total Ascii : "+ "   " +url11.getTotal()+" " );
		totalUrl(url11);
		frame.extendResult("Group : "+ "   " +url11.getTotal()+" " + "\n");
		
		
		Url url12 = new Url("http://www.hotmail.com/");
		castToAscii(url12, "http://www.hotmail.com/");
		frame.extendResult("URL :" + "12." + "  " + url12.getCharacter() + "    " + "Total Ascii : "+ "   " +url12.getTotal()+" " );
		totalUrl(url12);
		frame.extendResult("Group : "+ "   " +url12.getTotal()+" " + "\n");
		
		
		
	}
	
	
	
	ActionListener list;
	AsciiFrame frame;
	
	public  int castToAscii(Url url,String character){
		int total;
		total = 0;
		for(char c: character.toCharArray()){
			total = total + (int) c;
		}
		url.setTotal(total);
		return total;
	}
	
	
	public int totalUrl(Url url){
		int sum; 
		sum = url.getTotal();
		sum = sum %4;
		url.setTotal(sum);
		return sum;
	}
	
	
	
	


}

























